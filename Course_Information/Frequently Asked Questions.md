---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.1'
      jupytext_version: 1.2.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

## Installing and running the server


### My Jupyter server has crashed, link does not work

If the link http://localhost:8888 does not work, your server may have crashed (or may be running on a different port if you accidentally started muliple servers). 

 If this is the case, you should go back to the Anaconda prompt window it was running in and do the following:

1. Type `Ctrl-C`
2. Run the command `jupyter notebook stop`
3. Run the command `jupyter notebook start`

If you cannot find the Anaconda prompt window anymore, you can also open a new Anaconda prompt window and do steps 2 and 3 above. 


### My files are not in my home directory, how do I get to them? 

If your files are stored outside of your home directory, for example if they are on your D: drive in windows, you will have to first change directories in the Anaconda prompt to that folder before you run the command `jupyter notebook`. 

For example, let's say your files are on the D: drive in a folder `D:\Python\Practicum`. First you should stop the notebook server (see steps 1 and 2 of "Restarting a notebook server") and then you can use the following commands in the Anaconda prompt: 

* `d:`
* `cd Python`
* `jupyter notebook`

This will start a notebook server with the "root directory" as `D:\Python`, which will allow you to access all the files in subfolders of `D:\Python`. 


### Why is my web browser going to http://localhost:8889/ (instead of 8888)?  

This can happen when you (accidentally) open multiple notebook servers. You can list all the notebook servers that are running using the following command at the Anaconda prompt:

`jupyter notebook list`

You can then close any extra ones you don't want by using the `stop` command with their "port" number (the four numbers after "localhost"). For example, to stop one running on port 8889, use:

`jupyter notebook stop 8889`


## Using Jupyter Notebooks


### My code cell sometimes changes to a text cell, how do we change this back?

You have probably changed the "cell type" from "Code" to  "Markdown". You can change it back using the menu toolbar dropdown box: 

<img src="cell_type.png"></img>

There also <a href=https://jupyter-notebook.readthedocs.io/en/stable/examples/Notebook/Notebook%20Basics.html#Keyboard-Navigation>keyboard shortcuts</a> for many things. 

Most likely, you pushing the `M` key while the cell was selected in "command" mode (blue border). You can change it back by pushing the `Y` key. 

```python

```
