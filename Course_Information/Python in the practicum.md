---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.1'
      jupytext_version: 1.2.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Python in the practicum

## Overview

As part of the "Inleidend Practicum" course, you will have 5 lectures in which we will teach you the basics of programming with python.

For doing this, we have created a set of 5 "Lecture Notebooks", with the topics:

* **Notebook 1: Python Basics**
* **Notebook 2: Functions**
* **Notebook 3: Program Flow Control**
* **Notebook 4: Scientific Computing with Numpy**
* **Notebook 5: Data in Python**

For the material of Notebooks 1-3, we have an assignment connected to each notebook,  and for Notebooks 4 and 5, there will be a final project that will cover the material in those notebooks:

* **Assignment 1 (Must pass)**
* **Assignment 2 (Must pass)**
* **Assignment 3 (Must pass)**
* **Final Project (100%)**

The assignments will be checked in class. You must have completed and passed the three assignments in order to be allowed to submit your final project. 

##  Pace

While there are 5 lectures and 5 notebooks, we encourage you to work at your own pace through the material. More experienced programmers may cover the material more quickly, perhaps with more than one notebook worth of material per lecture, while those with less (or no) programming experience may want to go through the material more slowly and finish the material at home.  

##  Final Grade

Your python grade will count for 18% of the practicum grade (1ECTS). You grade will be determined by the final project, but your project will only be graded if you have passed all three assignments,  which  must be checked by a TA or lecturer in the lecture sessions (more on this below). 

## What do I do with the "Lecture" notebooks? 

The lecture notebooks explain the concepts of the material we expect you to learn. They contain both "information" sections for you to read, as well as "code sections" that you can run to demonstrate these concepts live.

At the top of each notebook, we have created a summary of the learning objectives we aim to achieve.

What should I do? You should first take a look at the learning objectives at the top of the notebook to understand what we want to achieve, then you can read through the learning material and execute the code cells to see what happens.

The notebooks also contain exercises where you can test what you have learned. Some are easy, some are a bit trickier: if you get stuck with the exercises, you should not hesitate to ask a fellow student, a TA, or a lecturer for help.

Try it yourself! Feel free to add code cells to the notebook (using the "Insert" menu) to try things out. If you find something you don't understand, you are always welcome to ask a TA or a lecturer to have a look.

## Assignment Notebooks

For lectures 1-3, there are "Assignment Notebooks" which we will ask you to complete.

The Assignment notebooks are designed to test if you have learned and understood the material from the lecture notebooks and have achieved the learning objective we have defined. Once you think you have mastered the material of the lecture notebooks and have played with writing code in the Try it yourself code cells and the exercise cells, you should move on to the Assignment Notebooks.

For the assignment notebooks, you should write your answer in the provided cells. When you think you have the correct answers, bring your laptop to a TA or lecturer and have them check your code. If you code is correct and you can explain it properly, then the TA will approve your completion of that assignment and you be marked as a "Pass".

Our pass criteria are:

* The code achieves what is asked in the question
* The student is able to explain what the code is doing to the TA
* The code cells contains comments explaining what it is doing

If the TA determines that your assignment has not satisfied these criteria, then you can continue to work on it and have it checked again when you think it is correct.

Before the TA marks your assignment as a "Pass", you will be required to upload that code to the appropriate assignment folder on Brightspace.

The assignments must be checked and approved by a TA before the following deadlines:

* Assignment 1: End of session P3
* Assignment 2: End of session P4
* Assignment 3: End of session P5

Individual exceptions are possible for reasons such as illness in discussion with the lecturers.

### Am I allowed to ask for help? 

Yes, of course! You are welcome to ask the teacher, the TAs, and other students for help with both the material in the Lecture Notebooks and in the Assignment Notebooks.  

Note that the TAs will be happy to help you "debug" your code  and point you  in the right  direction,  they will not give you  the answers to the assignment questions. 

###  Am  I allowed to copy my friends code? 

No! While we encourage you  to ask fellow students for help, you are not allowed to directly cut-and-paste  they  code (or copy their notebook  files). At  the  end  of the course, we will be performing a specialized plagiarism  scan (designed for computer code) on the  submitted assignments  and any anomalies will  be investigated.  

## Final Project

In addition to the assignments related  to the core programming concepts, we will also  have a final assignment that will determining your the final grade. 

In this  final assignment, you will load, process, plot and analyze a dataset using python. You will create a lab-report-style notebook in which you summarize your findings and  you will submit this via Brightspace for grading by the TAs. 

The final projects will be submitted via Brightspace in an assignment folder with a deadline announced per student group. 

**Important:** Your final project will NOT be graded unless you have achieved a pass evaluation on all the assignments as described above. 

```python

```
